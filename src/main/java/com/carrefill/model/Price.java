package com.carrefill.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class Price implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1806475972593753083L;
	private Double price;
	private LocalDateTime firstFetchedTime;

}
