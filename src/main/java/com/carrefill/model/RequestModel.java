package com.carrefill.model;

import lombok.Data;

@Data
public class RequestModel {

	private Boolean fuelLid;
	private String city;

}
