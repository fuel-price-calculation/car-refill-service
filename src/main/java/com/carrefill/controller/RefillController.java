package com.carrefill.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carrefill.model.RequestModel;
import com.carrefill.serviceimpl.CarRefillServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/car")
public class RefillController {

	@Autowired
	private CarRefillServiceImpl carRefilService;

	@PostMapping("/refill-instruction")
	public void refilInstruction(@RequestBody RequestModel request) {
		log.info(":: Received Fuel Price Calculation request for city : " + request.getCity());
		carRefilService.receiveRequest(request);
	}

}
