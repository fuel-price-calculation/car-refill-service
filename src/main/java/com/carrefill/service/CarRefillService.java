package com.carrefill.service;

import com.carrefill.model.RequestModel;

public interface CarRefillService {

	public void receiveRequest(RequestModel request);

}
