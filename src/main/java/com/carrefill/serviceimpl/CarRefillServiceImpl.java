package com.carrefill.serviceimpl;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carrefill.model.RequestModel;
import com.carrefill.service.CarRefillService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CarRefillServiceImpl implements CarRefillService {

	@Autowired
	private PriceCache priceCache;
	private LocalDateTime startTime = null;
	private LocalDateTime endTime = null;

	@Override
	public void receiveRequest(RequestModel request) {
		if (!request.getFuelLid()) {
			endTime = LocalDateTime.now();
			Double price = priceCache.fetchprice(request.getCity()).getPrice();
			if (startTime != null) {
				calculatePrice(price, startTime, endTime);
				startTime = null;
			} else {
				log.info(":: Please open the FuelLid of the car to refill fuel. (FuelLid: true)");
			}
		} else {
			startTime = LocalDateTime.now();
			log.info(":: Refilling Fuel...");
		}
	}

	public void calculatePrice(Double price, LocalDateTime startTime2, LocalDateTime endTime2) {
		log.info(":: Calculating price ... ");
		DecimalFormat df = new DecimalFormat("0.00");
		Double fPrice = Double.parseDouble(df.format(price));
		Double fliters = Double.parseDouble(df.format(ChronoUnit.SECONDS.between(startTime2, endTime2) / 30.00));
		Double finalPrice = Double.parseDouble(df.format(fliters * fPrice));
		log.info(":: Total amount to be paid for {} liters of fuel at {} /litre = {} INR", fliters, fPrice, finalPrice);
	}

}
