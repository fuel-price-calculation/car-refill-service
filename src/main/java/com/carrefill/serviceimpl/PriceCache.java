package com.carrefill.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.carrefill.feignproxy.FeignProxy;
import com.carrefill.model.Price;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PriceCache {

	@Autowired
	private FeignProxy feign;

	@Cacheable(value = "oneDayCache", key = "#city")
	public Price fetchprice(String city) {
		log.info(":: Fetching fuel price for city : " + city);
		ResponseEntity<Price> fuelPrice = feign.getFuelPrice(city);
		return fuelPrice.getBody();
	}

	@Scheduled(cron = "0 0 * * *")
	@CacheEvict(value = "oneDayCache")
	public void cacheRemove() {

	}
}
