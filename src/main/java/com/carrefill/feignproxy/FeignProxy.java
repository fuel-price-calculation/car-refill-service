package com.carrefill.feignproxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.carrefill.config.FeignConfig;
import com.carrefill.model.Price;

@RibbonClient
@FeignClient(name = "xxiap010-mongo-service", url = "localhost:9092/fuel", configuration = FeignConfig.class)
public interface FeignProxy {

	@GetMapping("/price/{city}")
	public ResponseEntity<Price> getFuelPrice(@PathVariable String city);
}
